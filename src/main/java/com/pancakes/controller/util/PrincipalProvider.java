package com.pancakes.controller.util;

import com.pancakes.entity.UserEntity;
import com.pancakes.security.CustomUserDetails;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Created by Piotr Papaj on 22.04.2017.
 */
@Component
public class PrincipalProvider {
    public UserEntity getPersonEntity() {
        return ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getPersonEntity();
    }
}

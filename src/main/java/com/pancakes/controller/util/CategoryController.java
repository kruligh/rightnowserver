package com.pancakes.controller.util;

import com.pancakes.entity.ErrandCatEntity;
import com.pancakes.repositories.ErrandCatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Piotr Papaj on 22.04.2017.
 */
@RestController
@RequestMapping(value = "/category")
public class CategoryController {

    @Autowired
    ErrandCatRepository errandCatRepository;

    @RequestMapping(value = "/list")
    public @ResponseBody Iterable<ErrandCatEntity>  listCategories() {
        return errandCatRepository.findAll();
    }
}

package com.pancakes.controller;

import com.pancakes.controller.dto.LoginRequest;
import com.pancakes.controller.dto.LoginResponseDto;
import com.pancakes.controller.util.PrincipalProvider;
import com.pancakes.entity.*;
import com.pancakes.helpers.ActiveUserHelper;
import com.pancakes.helpers.BadRequestException;
import com.pancakes.repositories.ErrandRepository;
import com.pancakes.repositories.OfferRepository;
import com.pancakes.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    PrincipalProvider principalProvider;

    @Autowired
    OfferRepository offerRepository;

    @Autowired
    ErrandRepository errandRepository;

    @RequestMapping(value = "/{login}", method = RequestMethod.GET)
    public @ResponseBody
    UserEntity getUser(@PathVariable(value = "login") String login){
        return userRepository.findByLogin(login);
    }

    @RequestMapping(value = "/session/login", method = RequestMethod.POST)
    public @ResponseBody LoginResponseDto login(@RequestBody LoginRequest loginRequest) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginRequest.getLogin(), loginRequest.getPassword());
        Authentication authentication = authenticationManager.authenticate(authenticationToken);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        LoginResponseDto response = new LoginResponseDto();
        response.setToken(RequestContextHolder.getRequestAttributes().getSessionId());
        return response;
    }

    @RequestMapping(value = "/session/logout", method = RequestMethod.POST)
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(auth != null)
            new SecurityContextLogoutHandler().logout(request, response, auth);
    }

    @RequestMapping(value = "/session/getMyUser", method = RequestMethod.GET)
    public @ResponseBody UserEntity getMyUser(HttpServletRequest request, HttpServletResponse response) {
        return ActiveUserHelper.getActiveUser(userRepository, principalProvider);
    }

    @RequestMapping(value = "/offers/list", method = RequestMethod.GET)
    public @ResponseBody Iterable<OfferEntity> getUserOfferList() throws BadRequestException {
        UserEntity user = ActiveUserHelper.getActiveUser(userRepository, principalProvider);

        if(user == null)
            throw new BadRequestException("User not logged in");

        Iterable<ErrandEntity> errands = errandRepository.findByEmployerAndState(user, ErrandState.ACTIVE.name());

        return offerRepository.findByErrandInAndState(errands, OfferState.ACTIVE.name());
    }

    @RequestMapping(value = "/errands/list", method = RequestMethod.GET)
    public @ResponseBody Iterable<ErrandEntity> getUserErrands() throws BadRequestException {
        UserEntity user = ActiveUserHelper.getActiveUser(userRepository, principalProvider);

        if(user == null)
            throw new BadRequestException("User not logged in");

        return errandRepository.findByEmployer(user);

    }
}

package com.pancakes.controller.dto;

import com.pancakes.entity.ErrandEntity;

/**
 * ta brzydka klasa Created by Piotr PaPaj on 23.04.2017.
 */
public class ErrandEntityResponseDto {


    private ErrandEntity errand;
    private boolean isOwner;

    public void setErrand(ErrandEntity errand) {
        this.errand = errand;
    }

    public ErrandEntity getErrand() {
        return errand;
    }

    public void setIsOwner(boolean isOwner) {
        this.isOwner = isOwner;
    }

    public boolean isOwner() {
        return isOwner;
    }

    public void setOwner(boolean isOwner) {
        this.isOwner = isOwner;
    }
}

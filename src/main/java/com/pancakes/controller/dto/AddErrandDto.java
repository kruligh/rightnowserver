package com.pancakes.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Piotr Papaj on 22.04.2017.
 */
public class AddErrandDto {
    @JsonProperty(required = true)
    private Long cat_id;
    @JsonProperty(required = true)
    private String title;
    @JsonProperty(required = true)
    private String description;
    private Double price;
    @JsonProperty(required = true)
    private Long lat;
    @JsonProperty(required = true)
    private Long lon;
    private Long est_time;

    public Long getCat_id() {
        return cat_id;
    }

    public void setCat_id(Long cat_id) {
        this.cat_id = cat_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getLat() {
        return lat;
    }

    public void setLat(Long lat) {
        this.lat = lat;
    }

    public Long getLon() {
        return lon;
    }

    public void setLon(Long lon) {
        this.lon = lon;
    }

    public Long getEst_time() {
        return est_time;
    }

    public void setEst_time(Long est_time) {
        this.est_time = est_time;
    }
}

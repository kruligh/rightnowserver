package com.pancakes.controller.dto;

/**
 * Created by Piotr Papaj on 23.04.2017.
 */
public class AddMessageDto {
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

package com.pancakes.controller.dto;

/**
 * Created by kroli on 23.04.2017.
 */
public class LoginResponseDto {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}

package com.pancakes.controller.dto;

/**
 * Created by Piotr Papaj on 23.04.2017.
 */
public class AddOfferDto {
    private Float price;

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }
}

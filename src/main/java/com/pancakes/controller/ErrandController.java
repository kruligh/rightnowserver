package com.pancakes.controller;

import com.pancakes.controller.dto.AddErrandDto;
import com.pancakes.controller.dto.AddMessageDto;
import com.pancakes.controller.dto.AddOfferDto;
import com.pancakes.controller.dto.ErrandEntityResponseDto;
import com.pancakes.controller.util.PrincipalProvider;
import com.pancakes.entity.*;
import com.pancakes.helpers.ActiveUserHelper;
import com.pancakes.helpers.BadRequestException;
import com.pancakes.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/errand")
public class ErrandController {

    private final Long LATITUDE_DELTA = 100000L;
    private final Long LONGITUDE_DELTA = 100000L;

    @Autowired
    ErrandRepository errandRepository;

    @Autowired
    MessageRepository messageRepository;

    @Autowired
    PrincipalProvider principalProvider;

    @Autowired
    ErrandCatRepository errandCatRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    OfferRepository offerRepository;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public @ResponseBody Iterable<ErrandEntity> getErrandList(@RequestParam(name = "lat") Long latitude,
                                         @RequestParam(name = "lon") Long longitude,
                                         @RequestParam(name = "cat") String categoriesParam){
        List<String> categories = Arrays.asList(categoriesParam.split(","));
        return errandRepository
                .findByLatitudeBetweenAndLongitudeBetweenAndStateAndCategoryNameIn(latitude - LATITUDE_DELTA,
                        latitude + LATITUDE_DELTA,
                        longitude - LONGITUDE_DELTA,
                        longitude + LONGITUDE_DELTA,
                        ErrandState.ACTIVE.name(),
                        categories);
    }

    @RequestMapping(value = "/{errandId}",method = RequestMethod.GET)
    public @ResponseBody
    ErrandEntityResponseDto getErrand(@PathVariable(name = "errandId") long errandId){
        ErrandEntityResponseDto responseDto = new ErrandEntityResponseDto();
        ErrandEntity entity =  errandRepository.findByErrandId(errandId);
        responseDto.setErrand(entity);
        responseDto.setIsOwner(true);

        return responseDto;

    }

    @RequestMapping(value = "/{errandId}/messages",method = RequestMethod.GET)
    public @ResponseBody Iterable<MessageEntity> getErrandMessages(@PathVariable(name = "errandId") long errandId){
        return messageRepository.findByErrandErrandId(errandId);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<String> addErrand(@RequestBody AddErrandDto parameters) throws Exception {

        UserEntity user = ActiveUserHelper.getActiveUser(userRepository, principalProvider);

        if(user == null)
            throw new BadRequestException("User not logged in");

        ErrandCatEntity category = errandCatRepository.findOne(parameters.getCat_id());

        if(category == null)
            throw new BadRequestException("Could not find category");

        ErrandEntity entity = new ErrandEntity();

        entity.setCategory(category);
        entity.setTitle(parameters.getTitle());
        entity.setDesc(parameters.getDescription());
        entity.setPrice(parameters.getPrice());
        entity.setLatitude(parameters.getLat());
        entity.setLongitude(parameters.getLon());
        entity.setEst_time(parameters.getEst_time());
        entity.setEmployer(user);
        entity.setDate_added(new Date());
        entity.setState(ErrandState.ACTIVE.name());
        entity.setEmployee_finished(false);
        entity.setEmployer_finished(false);

        errandRepository.save(entity);

        return ResponseEntity.ok("SUCCESS");
    }

    @RequestMapping(value = "/{id}/score", method = RequestMethod.POST)
    public @ResponseBody String addErrandScore(@RequestBody Long score, @PathVariable(name = "id") Long errandId) throws BadRequestException {
        ErrandEntity errand = errandRepository.findOne(errandId);
        UserEntity employee = errand.getEmployee();
        UserEntity employer = ActiveUserHelper.getActiveUser(userRepository, principalProvider);

        if(!errand.getEmployer().getLogin().equals(employer.getLogin()))
            throw new BadRequestException("Cannot score, you are not the ownder");

        if(employee == null)
            throw new BadRequestException("Cannot score, no emploee");

        if(errand.getScore() != null)
            throw new BadRequestException("Cannot score twice.");

        errand.setScore(score);
        Long noScores = employee.getNo_scores();
        Float scoreNow = employee.getScore() * noScores;

        employee.setScore((float)((scoreNow + score) / (noScores + 1.0)));
        employee.setNo_scores(noScores + 1);

        userRepository.save(employee);

        return "SUCCESS";
    }

    @RequestMapping(value = "/{errand_id}/offer/list", method = RequestMethod.GET)
    public @ResponseBody Iterable<OfferEntity> listOffers(@PathVariable(name = "errand_id") Long errand_id) {
        return offerRepository.findByErrandErrandId(errand_id);
    }

    @RequestMapping(value = "/{errand_id}/offer", method = RequestMethod.POST)
    public @ResponseBody String makeOffer(@PathVariable(name = "errand_id") Long errand_id,
                                          @RequestBody AddOfferDto body) throws BadRequestException {

        UserEntity user = ActiveUserHelper.getActiveUser(userRepository, principalProvider);
        if(user == null)
            throw new BadRequestException("User not logged in");

        ErrandEntity errand = errandRepository.findOne(errand_id);

        if(errand == null)
            throw new BadRequestException("Errand doesn't exist");

        if(user.getLogin().equals(errand.getEmployer().getLogin()))
            throw new BadRequestException("Cannot employ yourself");

        OfferEntity offer = new OfferEntity();

        offer.setPrice(body.getPrice());
        offer.setEmployee(user);
        offer.setErrand(errand);
        offer.setState(OfferState.ACTIVE.name());


        offerRepository.save(offer);
        return "SUCCESS";
    }

    @RequestMapping(value = "{errand_id}/offer/{offer_id}/accept", method = RequestMethod.POST)
    public @ResponseBody String acceptOffer(@PathVariable(name = "errand_id") Long errand_id,
                                            @PathVariable(name = "offer_id") Long offer_id) throws BadRequestException {
        OfferEntity offer = offerRepository.findOne(offer_id);
        if(offer == null)
            throw new BadRequestException("Offer doesn't exist");

        ErrandEntity errand = errandRepository.findOne(errand_id);
        if(errand == null)
            throw new BadRequestException("Errand doesn't exist");

        if(errand.getEmployee() != null)
            throw new BadRequestException("Employee already assigned");

        UserEntity employee = ActiveUserHelper.getActiveUser(userRepository, principalProvider);

        if(errand.getEmployer().getLogin().equals(employee.getLogin()))
            throw new BadRequestException("Cannot employ yourself");

        errand.setEmployee(employee);
        errand.setState(ErrandState.INPROGRESS.name());
        offer.setState(OfferState.ACCEPTED.name());

        errandRepository.save(errand);
        offerRepository.save(offer);

        return "SUCCESS";
    }

    @RequestMapping(value = "/{errand_id}/offer/{offer_id}/decline", method = RequestMethod.POST)
    public @ResponseBody String declineOffer(@PathVariable(name = "errand_id") Long errand_id,
                                            @PathVariable(name = "offer_id") Long offer_id) throws BadRequestException {
        OfferEntity offer = offerRepository.findOne(offer_id);
        if(offer == null)
            throw new BadRequestException("Offer doesn't exist");

        offer.setState(OfferState.DECLINED.name());

        offerRepository.save(offer);

        return "SUCCESS";
    }

    @RequestMapping(value = "/{errand_id}/finished", method = RequestMethod.POST)
    public @ResponseBody String finishedErrand(@PathVariable(name = "errand_id") Long errand_id)
            throws BadRequestException {
        ErrandEntity errand = errandRepository.findOne(errand_id);
        if(errand == null)
            throw new BadRequestException("Could not find errand");

        if(!errand.getState().equals(ErrandState.INPROGRESS.name()))
            throw new BadRequestException("Request is not in progress");

        UserEntity activeUser = ActiveUserHelper.getActiveUser(userRepository, principalProvider);

        if(errand.getEmployer().getLogin().equals(activeUser.getLogin())) {
            errand.setEmployer_finished(true);
        } else if(errand.getEmployee().getLogin().equals(activeUser.getLogin())) {
            errand.setEmployee_finished(true);
        } else {
            throw new BadRequestException("You are not a part of this request");
        }

        if(errand.getEmployee_finished() && errand.getEmployer_finished())
            errand.setState(ErrandState.DONE.name());

        errandRepository.save(errand);

        return "SUCCESS";
    }

    @RequestMapping(value = "/{errand_id}/messages", method = RequestMethod.POST)
    public @ResponseBody String addMessage(@PathVariable(name = "errand_id") Long errand_id,
                                           @RequestBody AddMessageDto body) throws BadRequestException {
        UserEntity activeUser = ActiveUserHelper.getActiveUser(userRepository, principalProvider);

        if(activeUser == null)
            throw new BadRequestException("User not logged in");

        ErrandEntity errand = errandRepository.findOne(errand_id);

        if(errand == null)
            throw new BadRequestException("Errand does not exist");

        MessageEntity message = new MessageEntity();

        message.setErrand(errand);
        message.setUser(activeUser);
        message.setDate_added(new Date());
        message.setText(body.getText());

        messageRepository.save(message);

        return "SUCCESS";
    }
}

package com.pancakes.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Piotr Papaj on 22.04.2017.
 */
@Entity
@Table(name = "messages")
public class MessageEntity {
    @Id
    @GeneratedValue
    private Long message_id;

    @OneToOne
    @JoinColumn(name = "ERRAND_ID")
    private ErrandEntity errand;

    @OneToOne
    @JoinColumn(name = "USER_LOGIN")
    private UserEntity user;

    private Date date_added;
    private String text;

    public Long getMessage_id() {
        return message_id;
    }

    public void setMessage_id(Long message_id) {
        this.message_id = message_id;
    }

    public ErrandEntity getErrand() {
        return errand;
    }

    public void setErrand(ErrandEntity errand) {
        this.errand = errand;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public Date getDate_added() {
        return date_added;
    }

    public void setDate_added(Date date_added) {
        this.date_added = date_added;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

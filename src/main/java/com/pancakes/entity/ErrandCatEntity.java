package com.pancakes.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Piotr Papaj on 22.04.2017.
 */
@Entity
@Table(name = "errand_cat")
public class ErrandCatEntity {
    @Id
    private Long cat_id;
    private String name;

    public Long getCat_id() {
        return cat_id;
    }

    public void setCat_id(Long cat_id) {
        this.cat_id = cat_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package com.pancakes.entity;

/**
 * Created by Piotr Papaj on 22.04.2017.
 */
public enum OfferState {
    ACTIVE,
    ACCEPTED,
    DECLINED
}

package com.pancakes.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ERRAND")
public class ErrandEntity {

    @Id
    @GeneratedValue
    @Column(name = "errandId")
    private Long errandId;

    @OneToOne
    @JoinColumn(name = "CAT_ID")
    private ErrandCatEntity category;
    private String state;
    private String title;
    @Column(name = "`desc`")
    private String desc;
    private Long latitude;
    private Long longitude;
    private Double price;
    private Long est_time;
    private Date date_added;
    private Date date_accepted;
    private Boolean employer_finished;
    private Boolean employee_finished;
    private Long score;

    @ManyToOne
    @JoinColumn(name = "EMPLOYER")
    private UserEntity employer;

    @ManyToOne
    @JoinColumn(name = "EMPLOYEE")
    private UserEntity employee;

    public Long getErrandId() {
        return errandId;
    }

    public void setErrandId(Long errandId) {
        this.errandId = errandId;
    }

    public ErrandCatEntity getCategory() {
        return category;
    }

    public void setCategory(ErrandCatEntity category) {
        this.category = category;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Long getLatitude() {
        return latitude;
    }

    public void setLatitude(Long latitude) {
        this.latitude = latitude;
    }

    public Long getLongitude() {
        return longitude;
    }

    public void setLongitude(Long longitude) {
        this.longitude = longitude;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getEst_time() {
        return est_time;
    }

    public void setEst_time(Long est_time) {
        this.est_time = est_time;
    }

    public Date getDate_added() {
        return date_added;
    }

    public void setDate_added(Date date_added) {
        this.date_added = date_added;
    }

    public Date getDate_accepted() {
        return date_accepted;
    }

    public void setDate_accepted(Date date_accepted) {
        this.date_accepted = date_accepted;
    }

    public Long getScore() {
        return score;
    }

    public void setScore(Long score) {
        this.score = score;
    }

    public UserEntity getEmployer() {
        return employer;
    }

    public void setEmployer(UserEntity employer) {
        this.employer = employer;
    }

    public UserEntity getEmployee() {
        return employee;
    }

    public void setEmployee(UserEntity employee) {
        this.employee = employee;
    }

    public Boolean getEmployer_finished() {
        return employer_finished;
    }

    public void setEmployer_finished(Boolean employer_finished) {
        this.employer_finished = employer_finished;
    }

    public Boolean getEmployee_finished() {
        return employee_finished;
    }

    public void setEmployee_finished(Boolean employee_finished) {
        this.employee_finished = employee_finished;
    }
}

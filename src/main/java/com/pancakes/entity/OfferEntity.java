package com.pancakes.entity;

import javax.persistence.*;

/**
 * Created by Piotr Papaj on 22.04.2017.
 */
@Entity
@Table(name = "offer")
public class OfferEntity {

    @Id
    @GeneratedValue
    private Long offer_id;
    private Float price;

    private String state;

    @OneToOne
    @JoinColumn(name = "ERRAND_ID")
    private ErrandEntity errand;

    @OneToOne
    @JoinColumn(name = "EMPLOYEE")
    private UserEntity employee;

    public Long getOffer_id() {
        return offer_id;
    }

    public void setOffer_id(Long offer_id) {
        this.offer_id = offer_id;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public ErrandEntity getErrand() {
        return errand;
    }

    public void setErrand(ErrandEntity errand) {
        this.errand = errand;
    }

    public UserEntity getEmployee() {
        return employee;
    }

    public void setEmployee(UserEntity employee) {
        this.employee = employee;
    }
}

package com.pancakes.security;

import com.pancakes.entity.UserEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.List;

/**
 * Created by Piotr Papaj on 22.04.2017.
 */
public class CustomUserDetails extends User {
    private UserEntity user;

    public CustomUserDetails(UserEntity entity, List<GrantedAuthority> roles) {
        super(entity.getLogin(), entity.getPassword(), roles);
        this.user = entity;
    }

    public UserEntity getPersonEntity() {
        return this.user;
    }
}

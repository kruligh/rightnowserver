package com.pancakes.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;

import java.util.Collections;

/**
 * Created by Piotr Papaj on 22.04.2017.
 */
@Configuration
public class AuthenticationManagerConfig {
    @Bean
    @Autowired
    AuthenticationManager userAuthenticationManager(CustomUserDetailsService customUserDetailsService) {
        DaoAuthenticationProvider userAuthProvider = new DaoAuthenticationProvider();
        userAuthProvider.setUserDetailsService(customUserDetailsService);
        return new ProviderManager(Collections.singletonList(userAuthProvider));
    }
}

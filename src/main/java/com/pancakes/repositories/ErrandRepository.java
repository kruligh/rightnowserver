package com.pancakes.repositories;

import com.pancakes.entity.ErrandEntity;
import com.pancakes.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Piotr Papaj on 22.04.2017.
 */
public interface ErrandRepository extends CrudRepository<ErrandEntity, Long> {
    ErrandEntity findByErrandId(Long id);

    Iterable<ErrandEntity> findByLatitudeBetweenAndLongitudeBetweenAndStateAndCategoryNameIn(
            Long latMin,
            Long latMax,
            Long lonMin,
            Long lonMax,
            String state,
            List<String> category);

    Iterable<ErrandEntity> findByEmployerAndState(UserEntity employer, String state);
    Iterable<ErrandEntity> findByEmployer(UserEntity employer);
}

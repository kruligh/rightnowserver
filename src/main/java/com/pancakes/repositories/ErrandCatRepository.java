package com.pancakes.repositories;

import com.pancakes.entity.ErrandCatEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Piotr Papaj on 22.04.2017.
 */
public interface ErrandCatRepository extends CrudRepository<ErrandCatEntity, Long> {
    ErrandCatEntity findByName(String name);
}

package com.pancakes.repositories;

import com.pancakes.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Piotr Papaj on 22.04.2017.
 */
public interface UserRepository extends CrudRepository<UserEntity, String> {
    UserEntity findByLogin(String login);
}

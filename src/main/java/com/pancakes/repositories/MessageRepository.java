package com.pancakes.repositories;

import com.pancakes.entity.MessageEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Piotr Papaj on 22.04.2017.
 */
public interface MessageRepository extends CrudRepository<MessageEntity, Long> {
    Iterable<MessageEntity> findByErrandErrandId(Long errandId);
}

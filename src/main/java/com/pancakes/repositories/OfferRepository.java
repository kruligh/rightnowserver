package com.pancakes.repositories;

import com.pancakes.entity.ErrandEntity;
import com.pancakes.entity.OfferEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Piotr Papaj on 22.04.2017.
 */
public interface OfferRepository extends CrudRepository<OfferEntity, Long> {
    Iterable<OfferEntity> findByErrandErrandId(Long errandId);

    Iterable<OfferEntity> findByErrandInAndState(Iterable<ErrandEntity> errands,
                                                 String offerState);
}

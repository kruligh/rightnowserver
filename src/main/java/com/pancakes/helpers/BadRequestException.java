package com.pancakes.helpers;

/**
 * Created by Piotr Papaj on 22.04.2017.
 */
public class BadRequestException extends Exception {
    public BadRequestException(String message) {
        super(message);
    }
}

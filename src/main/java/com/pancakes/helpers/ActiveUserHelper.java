package com.pancakes.helpers;

import com.pancakes.controller.util.PrincipalProvider;
import com.pancakes.entity.UserEntity;
import com.pancakes.repositories.UserRepository;

/**
 * Created by Piotr Papaj on 22.04.2017.
 */
public class ActiveUserHelper {
    public static UserEntity getActiveUser(UserRepository repository, PrincipalProvider principalProvider) {
        return repository.findOne(principalProvider.getPersonEntity().getLogin());
    }
}

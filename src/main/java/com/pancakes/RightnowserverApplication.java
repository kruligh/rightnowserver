package com.pancakes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RightnowserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(RightnowserverApplication.class, args);
	}
}
